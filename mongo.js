var MongoClient = require('mongodb').MongoClient;
// var url = 'mongodb://localhost:27017/ENnoter';
var url = 'mongodb://kaoru0103:963852@ds111262.mlab.com:11262/ennote';
var status = require('./routes/status');
const expressToken = require('express-token');

mongo = {
	users:{
		sign:function(req,next){
			MongoClient.connect(url, function(err, db) {
				db.collection('users').findOne({uid:req.uid})
					.then(res=>{
						if(!res){
							db.collection('users').insert({
							uid:req.uid,
							name:req.name,
							email:req.email,
							img:req.img,
							reg_date:new Date()})
							.then(res=>{
								next(status['200']);
							});	
						}else{
							next(status['403']);
						}
					})
			});
		},
		login:function(req,next){
			MongoClient.connect(url, function(err, db) {
				db.collection('users').findOne({uid:req.uid})
					.then(res=>{
						if(res){
							expressToken.login({uid:req.uid}).then(token=>{
								next({token:token,status:status['200']});
							});
						}else{
							mongo.users.sign(req,next);
						}
					});
			});
		},
	},
	noter:{
		write:function(req,next){
			MongoClient.connect(url, function(err, db) {
				db.collection('noter').find({uid:req.uid,'word_data.word':req.word_data.word})
				.toArray()
					.then((res)=>{
						if(res.length<1){
							db.collection('noter').insert(req).then(r=>{
								next({status:status['200'],res:r});
							});	
						}else{
							db.collection('noter').update(
								{uid:req.uid,'word_data.word':req.word_data.word},
								req
								).then(r=>{
								next({status:status['200'],res:r});
							});
						}
					})
			});
		},
		read:function(req,next){
			MongoClient.connect(url, function(err, db) {
				db.collection('noter').find({uid:req.uid,'word_data.word':req.word})
				.toArray()
					.then((res)=>{
						if(res.length>0){
							res[0].status = status['200'];
							next(res[0]);
						}else{
							next(status['404']);
						}
					});
			});
		},
		listMonth:function(req,next){
			let calMonth = 12;
			let nowYear = new Date().getFullYear();
			if(nowYear==req.year){
				calMonth = new Date().getMonth()+1;
			}
			let monthList = [];
			MongoClient.connect(url, function(err, db) {
			var looper = function(){
				db.collection('noter')
				.findOne({uid:req.uid,'date.year':req.year,'date.month':calMonth,'word_data.isStar':true})
				.then(res=>{
					if(res){
						monthList.push(calMonth);
					}
					calMonth--;
					if(calMonth>0){
						looper();
					}else{
						next(monthList);
					}
				});
			};
			looper();
			});
		},
		list:function(req,next){
			MongoClient.connect(url, function(err, db) {
				db.collection('noter').find(req).sort({'date.time': -1}).toArray()
					.then(res=>{
						next({status:status['200'],res:res});
					})
			});
		},
		examRecord:function(req,next){
			MongoClient.connect(url, function(err, db) {
				db.collection('record').insert(req).then(r=>{
					next({status:status['200'],res:r});
				})
			});
		},
		getRecord:function(req,opt,next){
			MongoClient.connect(url,function(err,db){
				db.collection('record').find(req,opt).skip(opt.skip).sort({'date': -1}).limit(opt.limit).toArray()
					.then(r=>{
						next({status:status['200'],res:r});
					});
			});
		}
	}

}
function getDB(connection){
	return connection.db('ENnoter');
}



module.exports = mongo;