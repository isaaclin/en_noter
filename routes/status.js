var status = {
	200:{
		status:'200',
		msg:'',
	},
	404:{
		status:'404',
		msg:"route or resource doesn't exist !",
	},
	403:{
		status:'403',
		msg:"requirement didn't allowed",
	},
	500:{
		status:'500',
		msg:"wrong params",
	},
	505:{
		status:'505',
		msg:"login error",	
	}
}
module.exports = status;