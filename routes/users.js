var express = require('express');
var router = express.Router();
const expressToken = require('express-token');
var status = require('./status');

var mongo = require('./../mongo');


/* GET users listing. */
router.get('/', function(req, res, next) {
  apiIntro = {
  	users:{
  		login:{uid:'string'},
  		logout:'',
  	}
  };
  res.send(apiIntro);
});

router.post('/login',function(req,res){
	mongo.users.login(req.body,obj=>{
		res.send(obj);
	});
});
router.post('/sign',function(req,res){
	let obj_req = {
		uid:null,
		user_name:null,
		user_email:null,
	};
	let mongo_req = {};
	for(let i in obj_req){
		if(req.body[i]){
			mongo_req[i] = req.body[i]	
		}else{
			let msg = status['500'];
			msg.info = i;
			res.send(msg);
			return false;
		}
	}
	let date = new Date();
	mongo_req.date = date.getFullYear()+'/'+date.getMonth()+'/'+date.getDate();
	mongo.users.sign(mongo_req,function(status){
		res.send(status);
	})
});

module.exports = router;
