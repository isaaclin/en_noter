var express = require('express');
var router = express.Router();
var status = require('./status');
var mongo = require('./../mongo');

/* GET noter listing. */
router.get('/', function(req, res, next) {
  apiIntro = {
  	noter:{
  	}
  };
  res.send(apiIntro);
});

router.post('/list/month',function(req,res){
	let obj_req = {
		year:null
	};
	for(let i in obj_req){
		if(req.body[i]){
			obj_req[i] = req.body[i]	
		}else{
			send500(res,i);
		}
	}
	obj_req.uid = check_uid(req,res);
	if(obj_req.uid){
		mongo.noter.listMonth(obj_req,(obj)=>{
			res.send(obj);
		});	
	}
});

router.post('/list',function(req,res){
	let obj_req = req.body;
	obj_req.uid = check_uid(req,res);
	mongo.noter.list(obj_req,obj=>{
		res.send(obj);
	})
});

router.post('/read',function(req,res){
	let obj_req = {word:null,};
	for(let i in obj_req){
		if(req.body[i]){
			obj_req[i] = req.body[i]	
		}else{
			send500(res,i);
		}
	}
	obj_req.uid = check_uid(req,res);
	if(obj_req.uid){
		mongo.noter.read(obj_req,(obj)=>{
			res.send(obj);
		});	
	}

})

router.post('/write',function(req,res){
	let obj_req = {word_data:null,};
	for(let i in obj_req){
		if(req.body[i]){
			obj_req[i] = req.body[i]	
		}else{
			send500(res,i);
		}
	}
	let date = new Date();
	obj_req.date = {
		year:date.getFullYear(),
		month:date.getMonth()+1,
		date:date.getDate(),
		time:date.getTime(),
	}
	obj_req.uid = check_uid(req,res);
	if(obj_req.uid){
			mongo.noter.write(obj_req,(obj)=>{
			res.send(obj);
		});	
	}
});
router.post('/examRecord',function(req,res){
	let obj_req = req.body;
	obj_req.date = new Date();
	obj_req.uid = check_uid(req,res);
	if(obj_req.uid){
		mongo.noter.examRecord(obj_req,(obj)=>{
			res.send(obj);
		});	
	}
});
router.post('/getRecord',function(req,res){
	let obj_req = {};
	let opt = {
		limit:req.body.perPage,
		skip:req.body.perPage*req.body.page,
	}
	obj_req.uid = check_uid(req,res);
	if(obj_req.uid){
		mongo.noter.getRecord(obj_req,opt,(obj)=>{
			res.send(obj);
		});	
	}
});
function check_uid(req,res){
	if(req.user){
		return req.user.uid;
	}else{
		send500(res);
		return false;
	}
}

// function org_date(obj_req){
// 	if(Array.isArray(obj_req.date)){
// 		var temp_obj = {};
// 		temp_obj.year = {$gte:obj_req.date[0]['year'],$lte:obj_req.date[1]['year']};
// 		temp_obj.month = {$gte:obj_req.date[0]['month'],$lte:obj_req.date[1]['month']};
// 		temp_obj.date = {$gte:obj_req.date[0]['date'],$lte:obj_req.date[1]['date']};
// 		var obj = {}
// 		for(var i in temp_obj){
// 			if(temp_obj[i]['$gte']){
// 				obj_req['date.'+i]=temp_obj[i];
// 			}
// 		}
// 	}else{
// 		for(var i in obj_req.date){
// 			obj['date.'+i]=date[i];
// 		}
// 	}
// }
function send500(res,i){
	let msg = status['500'];
	msg.info = i || null;
	res.send(msg);
}

module.exports = router;
